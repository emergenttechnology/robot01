/*
 * Basic Robot Skills
 * Christopher Moran
 * February 2017
 * 
 * Assumptions:
 * 1. This is a two-wheel device which uses differential steering.
 * 2. Motor control is a generic 2 channel power driver.  One channel for each motor.
 * 3. There are 3 IR Receivers used to measure reflection and thus test for walls.
 * 
 */

// This line invokes testing functions.
// you don't want to do this for real applications
//#define _TEST
// This line will configure debug output via serial
// port, and add debugging prints to the code.
#define _DEBUG

#define LED_PIN        13
#define MOTOR_A_ENABLE 10     // Left Side
#define MOTOR_A_IN_1    9
#define MOTOR_A_IN_2    8
#define MOTOR_B_ENABLE  5     // Right Side
#define MOTOR_B_IN_1    7
#define MOTOR_B_IN_2    6
#define CONTACT_ENABLE  11
#define CONTACT_0       (A0)  // Centre
#define CONTACT_1       (A1)  // Left Side
#define CONTACT_2       (A2)  // Right Side
#define VBATT           (A3)  // Measure battery voltage

#define IR_THRESHOLD    512

bool state;

void motorAForward() {
  digitalWrite(MOTOR_A_IN_1, HIGH);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorAReverse() {
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, HIGH);
}

void motorAStop() {
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorASpeed(int val) {
  analogWrite(MOTOR_A_ENABLE, val);
}

void motorBForward() {
  digitalWrite(MOTOR_B_IN_1, HIGH);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBReverse() {
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, HIGH);
}

void motorBStop() {
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBSpeed(int val) {
  analogWrite(MOTOR_B_ENABLE, val);
}

int readBattery() {
  return analogRead(VBATT);
}

/*
 * These test functions are static motor tests
 * They will move in a straight line, exercising
 * the motor controller functions.
 */
#ifdef _TEST
void test1() {
#ifdef _DEBUG
  Serial.println("Begin test1()");
#endif
  // Turn on motor A
  motorAForward();
  // Turn on motor B
  motorBForward();
  // Set speed to 255 / 255 (pretty fast)
  motorASpeed(255);
  motorBSpeed(255);

  delay(2000);

  // Change motor direction
  motorAReverse();
  motorBReverse();
  delay(2000);

  // Everything off
  motorAStop();
  motorBStop();
#ifdef _DEBUG
  Serial.println("End test1()");
#endif
}

void test2() {
#ifdef _DEBUG
  Serial.println("Begin test2()");
#endif
  motorAForward();
  motorBForward();
  // Accelerate from 0 to maximum
  for(int i = 0; i < 256; i++){
    motorASpeed(i);
    motorBSpeed(i);
    delay(50);
  }
  delay(1000);
  //Decelerate in the same fashion
  for(int i = 255; i >= 0; i--){
    motorASpeed(i);
    motorBSpeed(i);
    delay(50);
  }
  // Everything off
  motorAStop();
  motorBStop();
#ifdef _DEBUG
  Serial.println("End test2()");
#endif
}

void test3() {
#ifdef _DEBUG
  Serial.println("Begin test3()");
#endif
  motorAForward();
  motorBReverse();
    motorASpeed(64);
    motorBSpeed(64);
  delay(2000);
    motorASpeed(0);
    motorBSpeed(0);
  motorAStop();
  motorBStop();  
#ifdef _DEBUG
  Serial.println("End test3()");
#endif
}

void test4() {
#ifdef _DEBUG
  Serial.println("Begin test4()");
#endif
  motorAReverse();
  motorBForward();
    motorASpeed(64);
    motorBSpeed(64);
  delay(2000);
    motorASpeed(0);
    motorBSpeed(0);
  motorAStop();
  motorBStop();  
#ifdef _DEBUG
  Serial.println("End test4()");
#endif
}
#endif

bool pollSensor(int id) {
  int i;
  int r;
  bool ret = false;
  
#ifdef _DEBUG
  Serial.print("Begin pollSensor(");
  Serial.print(id);
  Serial.println(")");
#endif
  // Turn on the sensor when we want to check the distance
  digitalWrite(CONTACT_ENABLE, HIGH);
  delayMicroseconds(220);
  r = analogRead(id);
#ifdef _DEBUG
  Serial.print("pollSensor(");
  Serial.print(id);
  Serial.print(") Receive = ");
  Serial.println(r);
#endif
  if(r > IR_THRESHOLD) {
    ret = false;
  }
  else {
    delayMicroseconds(395);   // Debounce the result
    if(analogRead(id) > IR_THRESHOLD) {
      ret = false;
    }
    else
      ret = true;
  }
  digitalWrite(CONTACT_ENABLE, LOW);
  return ret;
}

void setup() {
  // We only use serial in debug builds.
#ifdef _DEBUG
  Serial.begin(38400);
  Serial.println("in setup()");
#endif
  pinMode(MOTOR_A_ENABLE, OUTPUT);
  pinMode(MOTOR_B_ENABLE, OUTPUT);
  pinMode(MOTOR_A_IN_1, OUTPUT);
  pinMode(MOTOR_A_IN_2, OUTPUT);
  pinMode(MOTOR_B_IN_1, OUTPUT);
  pinMode(MOTOR_B_IN_1, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(CONTACT_ENABLE, OUTPUT);
  state = false;
  randomSeed(analogRead(A3));
}

void loop() {
  digitalWrite(LED_PIN, state);
  /*
   * The purpose of _TEST is to allow me to run the controller, motors and sensors
   * for extended periods to check for any problems related to electrical noise or
   * other non-software factors.
   * 
   * I would suggest running in this mode for 24 hours as a solid "soak" test
   */
#ifdef _TEST
  delay(1000);
  test1();
  delay(1000);
  test2();
  delay(1000);
  test3();
  delay(1000);
  test4();
  delay(1000);
  pollSensor(CONTACT_0);
  delay(500);
  pollSensor(CONTACT_1);
  delay(500);
  pollSensor(CONTACT_2);
  delay(500);
#else
  /*
   * Heisermann's 'Alpha' behaviour model
   * 
   * The only strategy in play here is avoiding objects
   */
   switch(random(0,3)) {
    case 0:
      motorAStop();
      break;
    case 1:
      motorAForward();
      motorASpeed(128);
      break;
    case 2:
      motorAReverse();
      motorASpeed(128);
      break;
    default:
      motorAStop();
      break;
   }
   switch(random(0,3)) {
    case 0:
      motorBStop();
      break;
    case 1:
      motorBForward();
      motorBSpeed(128);
      break;
    case 2:
      motorBReverse();
      motorBSpeed(128);
      break;
    default:
      motorBStop();
      break;
   }
   /*
    * At this point, the motor direction is set up, but we aren't moving.
    * This will happen in the next loop.
    */
#ifdef _DEBUG
   Serial.println("loop():\tWaiting to hit something");
#endif
   while((pollSensor(CONTACT_0) == false) && (pollSensor(CONTACT_1) == false) && (pollSensor(CONTACT_2) == false)) {
    // This will probably make the robot walk like a drunken sailor, but it will be amusing at least
//    motorASpeed(random(128,256));
//    motorBSpeed(random(128,256));
    motorASpeed(128);
    motorBSpeed(128);
    // 100mS delay here will probably smooth the speed changes out a little.
    // but we might still be running at an angle.
    delay(100);
   }
#ifdef _DEBUG
   Serial.println("loop():\tCollision detected.\tFinding a way out");
#endif
   // Hit the brakes
   motorASpeed(0);
   motorBSpeed(0);
   motorAStop();
   motorBStop();
#endif

  state = state ^ 1;
}

